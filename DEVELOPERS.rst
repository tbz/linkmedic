*****************
Developer's Guide
*****************


Packaging and Dependency Management
###################################

* Install PDM:

.. code-block:: shell

  pip install pdm

* Install project dependencies in the PDM environment:

.. code-block:: shell

  pdm install

* Run ``linkmedic`` (in the PDM environment):

.. code-block:: shell

  pdm run linkmedic

* Build the package:

.. code-block:: shell

  pdm build

* Publish the built package:

.. code-block:: shell

  pdm publish --no-build

* View the dependency graph:

.. code-block:: shell

  pdm list --graph

* Add a new dependency:

.. code-block:: shell

  pdm add <pkg_name>

* Update dependencies and lockfile:

.. code-block:: shell

  pdm update
  pdm update --update-eager

* Update the lockfile:

.. code-block:: shell

  pdm lock --group :all --refresh

Styling
#######
To maintain consistent styling, run the following commands:

.. code-block:: shell

  black linkmedic
  isort linkmedic

Container
#########
The container image of ``linkmedic`` is built using the tested wheel of the ``linkmedic`` package in CI. It also requires the ``linkmedkit`` tools, which are included as a submodule in this Git repository.

To build the container image locally:

* Clone the Git repository and pull the submodules:

.. code-block:: shell

  git submodule update --init --recursive

* Build the package:

.. code-block:: shell

  pdm build

* Build the container image using your preferred builder, passing ``VERSION`` (e.g., ``0.7.0``) as an argument:

.. code-block:: shell

  podman build -t linkmedic --build-arg "VERSION=0.7.0" .

Testing
#######
To run the tests:

* Install the required packages in the PDM environment:

.. code-block:: shell

  pdm install

* Run the tests in the PDM environment:

.. code-block:: shell

  tests/linkmedic_check.sh --launcher="pdm run"

Alternatively, to run the tests on the ``linkmedic`` in your path, simply run the test script:

.. code-block:: shell

  tests/linkmedic_check.sh
