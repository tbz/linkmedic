FROM almalinux:9.5
ENV PATH="/linkmedkit:${PATH}"
ARG COMMIT_HASH='None'
ARG VERSION='0.0.0'
ARG BUILD_TIME='Undefined'

WORKDIR /linkmedic
COPY dist README.rst LICENSE ./

WORKDIR /linkmedkit
COPY linkmedkit ./

RUN yum install -y python3 python3-pip jq which && \
    yum clean all && \
    pip3 install /linkmedic/linkmedic-${VERSION}-py3-none-any.whl && \
    pip3 install -r /linkmedkit/requirements.txt && \
    pip3 freeze

RUN groupadd -r medic && useradd -m --no-log-init -r -g medic medic
USER medic

LABEL org.opencontainers.image.title="LinkMedic"
LABEL org.opencontainers.image.description="Website link checker"
LABEL org.opencontainers.image.source="https://gitlab.mpcdf.mpg.de/tbz/linkmedic.git"
LABEL org.opencontainers.image.documentation="https://tbz.pages.mpcdf.de/linkmedic/"
LABEL org.opencontainers.image.revision="${COMMIT_HASH}"
LABEL org.opencontainers.image.version="${VERSION}"
LABEL org.opencontainers.image.created="${BUILD_TIME}"
LABEL org.opencontainers.image.authors="M. Farzalipour Tabriz"
LABEL org.opencontainers.image.licenses="BSD-3-Clause"
