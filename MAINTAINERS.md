# Maintainers guide

## Release checklist

1. Update dependencies and lockfile (See `DEVELOPERS.rst`). Push your changes and verify all the CI tests.
1. Verify minimum supported python version (update in `README.rst`, `.gitlab-ci.yml` and `pyproject.toml` if necessary). Drop EOL python versions, and add new ones to CI build and test.
1. Update crawler's user-agent version in `linkmedic/diagnose.py` based on the latest FireFox.
1. If you have not done it yet; import your X509 certificate to gpgsm `gpgsm --import smime_XXXX.p12`. Get your key ID `gpgsm -k`. Configure git for signing tags with your key:

    ```bash
    git config --local user.signingkey YOUR_KEY_ID
    git config --local gpg.format x509
    git config --local tag.gpgSign true
    ```

1. Tag the commit with new version e.g. `v1.2` as `git tag -s -m "v1.2" v1.2`. Your committer email must match the email in your certificate. Otherwise, GitLab won't verify the signature. You can set your email explicitly for a single tag as `GIT_COMMITTER_EMAIL="user@example.com" git tag -s -m "v1.2" v1.2`
1. Verify the signature locally with `git verify-tag v1.2`
1. The root certificate must be marked as trusted in [verification environment](https://gitlab.mpcdf.mpg.de/tbz/ci_tools). Update it if necessary.
1. Push tags `git push origin --tags`. This should automatically build and push the package to MPCDF GitLab package repository.
1. Check [GitLab's signature verification](https://gitlab.mpcdf.mpg.de/tbz/linkmedic/-/tags) on new tag.
1. Manually trigger `test:package:mpcdf` job.
1. Generate a PyPI API token, manually set the value of `PDM_PUBLISH_PASSWORD` CI variable to this new token and trigger  `release:package:pypi` job in CI/CD pipeline. Remove token from the PyPI afterwards.
1. Manually trigger `test:package:pypi` CI job.
1. [Create a new release on GitLab](https://gitlab.mpcdf.mpg.de/tbz/linkmedic/-/releases/new) based on the new tag. Leave release title empty. Add all the relevant info about the changes in release notes. Use [compare revisions](https://gitlab.mpcdf.mpg.de/tbz/linkmedic/-/compare) to find all changes between the new release and the one before it.
